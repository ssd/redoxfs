#![no_main]

use std::collections::HashMap;
use std::ops::DerefMut;
use std::path::{Component, Path, PathBuf};
use std::process::Command;
use std::{fs, sync, thread, time};

use redoxfs::{unmount_path, DiskSparse, FileSystem};

use libfuzzer_sys::fuzz_target;
use libfuzzer_sys::arbitrary;
use libfuzzer_sys::arbitrary::Arbitrary;

#[derive(Arbitrary, Debug, Clone)]
enum FilesystemAction {
    CreateFile {
        path: String,
        contents: String,
    },
    WriteFile {
        index: u32,
        contents: String,
    },
    ReadFile { // and verify
        index: u32,
    },
    DeleteFile {
        index: u32
    },


    //read/delete/rename file that doesn't exist
}
// VERY slow
fuzz_target!(|actions: Vec<FilesystemAction>| {
    with_redoxfs(move |real_path| {
        let mut paths = vec![];
        let mut hashmap = HashMap::new();
        for action in &actions {
            match action {
                FilesystemAction::CreateFile { path, contents } => {
                    if path.is_empty() {
                       continue;
                    }
                    if path.len() >= 252 {
                        continue;
                    }
                    if PathBuf::from(path).components().any(| x | x == Component::ParentDir) {
                        continue;
                    }
                    if !path.is_ascii() || path.contains(| ch: char | !ch.is_alphanumeric() ) { // || path.contains(&['\0', '\n', '\t'])
                        continue;
                    }

                    if contents.contains('\0') {
                        continue;
                    }
                    let path = real_path.join(path);
                    if paths.contains(&path) {
                        continue;
                    }

                    fs::write(&path, contents).unwrap();
                    hashmap.insert(path.clone(),  contents);
                    paths.push(path);
                },
                FilesystemAction::WriteFile { index, contents } => {
                    if paths.len() == 0 {
                        continue;
                    }
                    if contents.contains('\0') {
                        continue;
                    }


                    let path = &paths[*index as usize % paths.len()];
                    hashmap.insert(path.clone(),  contents);
                    fs::write(path, contents).unwrap();

                },
                FilesystemAction::ReadFile { index } => {
                    if paths.len() == 0 {
                        continue;
                    }

                    let path = &paths[*index as usize % paths.len()];
                    let contets = fs::read_to_string(path).unwrap();
                    let real_contets = hashmap.get(path).unwrap().to_string();

                    assert_eq!(contets, real_contets);

                },
                FilesystemAction::DeleteFile { index } => {
                    if paths.len() == 0 {
                        continue;
                    }

                    let path = &paths[*index as usize % paths.len()].clone();
                    paths.remove(*index as usize % paths.len());
                    fs::remove_file(path).unwrap();
                    hashmap.remove(path);
                },
                 //_ => ()
            }
        }
    })
});


fn with_redoxfs<T, F>(callback: F) -> T
where
    T: Send + Sync + 'static,
    F: FnMut(&Path) -> T + Send + Sync + 'static,
{
    let disk_path = "/tmp/fuzzer-image.img";
    let mount_path = "/tmp/image";

    let res = {
        let disk = DiskSparse::create(disk_path, 1024 * 1024 * 5).unwrap();

        if cfg!(not(target_os = "redox")) {
            if !Path::new(mount_path).exists() {
                (fs::create_dir(mount_path)).unwrap();
            }
        }

        let ctime = (time::SystemTime::now().duration_since(time::UNIX_EPOCH)).unwrap();
        let fs = FileSystem::create(disk, None, ctime.as_secs(), ctime.subsec_nanos()).unwrap();

        let callback_mutex = sync::Arc::new(sync::Mutex::new(callback));
        let join_handle = redoxfs::mount(fs, mount_path, move |real_path| {
            let callback_mutex = callback_mutex.clone();
            let real_path = real_path.to_owned();
            thread::spawn(move || {
                let res = {
                    let mut callback_guard = callback_mutex.lock().unwrap();
                    let callback = callback_guard.deref_mut();
                    callback(&real_path)
                };

                if cfg!(target_os = "redox") {
                    fs::remove_file(format!(":{}", mount_path)).unwrap();
                } else {
                    while !(Command::new("sync").status()).unwrap().success() {

                    }

                    while !unmount_path(mount_path).is_ok() {

                    }
                }

                res
            })
        })
        .unwrap();

        join_handle.join().unwrap()
    };

    fs::remove_file(disk_path).unwrap();

    if cfg!(not(target_os = "redox")) {
        fs::remove_dir(mount_path).unwrap();
    }

    res
}

