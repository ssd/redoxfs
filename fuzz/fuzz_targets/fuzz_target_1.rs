#![no_main]
#![feature(io_error_uncategorized)]

mod helper;

use std::path::Path;
use std::{fs, io, time};
use std::io::ErrorKind;

use redoxfs::{DiskSparse, FileSystem, Node, TreePtr};

use libfuzzer_sys::arbitrary::Arbitrary;
use libfuzzer_sys::arbitrary;

#[derive(Arbitrary, Debug, Clone)]
enum FilesystemAction {
    CreateFile {
        path: String,
        contents: String,
    },
    WriteFile {
        index: u32,
        contents: String,
    },
    ReadFile { // and verify
        index: u32,
    },
    DeleteFile {
        index: u32
    },


    //read/delete/rename file that doesn't exist
}



impl FilesystemAction {
    fn run<'a>(&'a self, fs: &mut FileSystem<DiskSparse>, paths: &mut Vec<&'a Path>) -> Result<(), io::Error> {

        let error = fs.tx(|tx| {
        let metadata = helper::Metadata::default();

         match self {
            FilesystemAction::CreateFile { path, contents } => {
                if !path.is_ascii() || path.contains(| ch: char | !ch.is_alphanumeric() ) { // || path.contains(&['\0', '\n', '\t'])
                    return Ok(None);
                }
                if path.len() >= 252 {
                    return Ok(None);
                }

                if contents.contains('\0') {
                    return Ok(None);
                }

                let path = Path::new(path);
                if path.file_name().is_none() {
                    return Ok(None);
                }
                if paths.contains(&path) {
                    return Ok(None)
                }

                paths.push(path);

                match helper::create_inner(path.as_ref(), Node::MODE_FILE, &metadata, tx) {
                    Ok(node_ptr) => {
                        let data = contents.as_bytes();
                        let count = tx
                            .write_node(
                                node_ptr,
                                0,
                                &data,
                                metadata.mtime,
                                metadata.mtime_nsec,
                            ).unwrap();

                        if count != data.len() {
                            panic!("file write count {} != {}", count, data.len());
                        }
                    },
                    Err(e) => { return Ok(Some(e)) },
                };

            },
            FilesystemAction::WriteFile { index, contents } => {
                if paths.len() == 0 {
                    return Ok(None);
                }

                if contents.contains('\0') {
                    return Ok(None);
                }

                let path = paths[*index as usize % paths.len()];
                let name = match path.file_name() {
                    Some(s) => s,
                    None => panic!("Path is wrong"),
                }.to_str().unwrap();

                let mut children = vec![];
                tx.child_nodes(TreePtr::root(), &mut children).unwrap();

                let child = children.into_iter().find(| x | x.name().unwrap() == name).unwrap();

                let data = contents.as_bytes();
                let count = tx
                    .write_node(
                        child.node_ptr(),
                        0,
                        &data,
                        metadata.mtime,
                        metadata.mtime_nsec,
                    ).unwrap();

                if count != data.len() {
                    panic!("file write count {} != {}", count, data.len());
                }

            },
            /*FilesystemAction::DeleteFile { index } => {
                if paths.len() == 0 {
                    return Ok(None);
                }
                let path = paths[*index as usize % paths.len()];
                let name = match path.file_name() {
                    Some(s) => s,
                    None => panic!("Path is wrong"),
                }.to_str().unwrap();

                let mut children = vec![];
                tx.child_nodes(TreePtr::root(), &mut children)?;

                let child = children.into_iter().find(| x | x.name().unwrap() == name).unwrap();

                tx.remove_node(child.node_ptr(), child.name().unwrap(), Node::MODE_FILE)?;
                paths.remove(*index as usize % paths.len());
            },*/
            FilesystemAction::ReadFile { index } => {
                if paths.len() == 0 {
                    return Ok(None);
                }
                let path = paths[*index as usize % paths.len()];
                let name = match path.file_name() {
                    Some(s) => s,
                    None => panic!("Path is wrong"),
                }.to_str().unwrap();

                let mut children = vec![];
                tx.child_nodes(TreePtr::root(), &mut children).unwrap();

                let child = children.into_iter().find(| x | x.name().unwrap() == name).unwrap();

                let mut buffer: [u8; 1024] = [0; 1024];
                tx.read_node(child.node_ptr(), 0, &mut buffer, 0, 0).unwrap();

                /*let string = match String::from_utf8(buffer.to_vec()) {
                    Ok(s) => s,
                    Err(e) => panic!("Contents are wrong error: {e:?}"),
                };*/
                // TODO: Validate
            },
            _ => ()

        }

        /*tx.sync(true)?;
        let end_block = tx.header.size() / BLOCK_SIZE;
        tx.header.size = (end_block * BLOCK_SIZE).into();
        tx.header_changed = true;*/
        tx.sync(true)?;
        Ok(None)

    }).map_err(|err| io::Error::from_raw_os_error(err.errno))?;

    match error {
        None => Ok(()),
        Some(e) => Err(e)
    }

    }
}



libfuzzer_sys::fuzz_target!(|actions: Vec<FilesystemAction>| {

    //panic!("{actions:#?}");
    let disk_path = "/tmp/redox_fuzzer.img";

    let _ = fs::remove_file(disk_path);

    let disk = DiskSparse::create(disk_path, 1024 * 1024 * 10).unwrap();
    let ctime = time::SystemTime::now().duration_since(time::UNIX_EPOCH).unwrap();
    let mut fs = FileSystem::create(disk, None, ctime.as_secs(), ctime.subsec_nanos()).unwrap();
    //helper::archive(&mut fs, "test").unwrap();

    let mut paths = vec![]; // index = index % paths.len()

    for action in &actions {
        match action.run(&mut fs, &mut paths) {
            Ok(_) => (),
            Err(e) => match e.kind() {
                ErrorKind::InvalidData => (),
                ErrorKind::InvalidInput => (),
                ErrorKind::AlreadyExists => (),
                ErrorKind::OutOfMemory => (),
                ErrorKind::Uncategorized => (),

                e => panic!("{e}")
            }
        };
    }
});





